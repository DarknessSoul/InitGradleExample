/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package initgradle.app;

import initgradle.list.LinkedList;

import static initgradle.utilities.StringUtils.join;
import static initgradle.utilities.StringUtils.split;
import static initgradle.app.MessageUtils.getMessage;

import org.apache.commons.text.WordUtils;

public class App {
    public static void main(String[] args) {
        LinkedList tokens;
        tokens = split(getMessage());
        String result = join(tokens);
        System.out.println(WordUtils.capitalize(result));
    }
}
